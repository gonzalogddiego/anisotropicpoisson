#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 21 14:21:32 2020

@author: gonzalo
"""

from firedrake import *
import numpy as np
from firedrake.petsc import PETSc
import solver_parameters
from transfer import SchoeberlProlongation
import argparse
from alfi.bary import BaryMeshHierarchy

parser = argparse.ArgumentParser(add_help=True)
parser.add_argument("--discretisation", type = str, required = True,
    choices = ["sv", "pkp0", "rt", "rt-quad"])
parser.add_argument("--relaxation", type=str, required=True,
    choices = ["jacobi", "line", "star", "lineandstar"])
parser.add_argument("--patch-composition", type=str, default = "additive",
    choices = ["additive", "multiplicative"])
parser.add_argument("--mg", dest="mg", default=False,
                    action="store_true")
parser.add_argument("--gamma", type=float, default=0)
parser.add_argument("--N", type=int, default=8)
parser.add_argument("--k", type=int, default=1)
parser.add_argument("--epsilon", type=float, default=1)
parser.add_argument("--nref", type=int, default=1)
parser.add_argument("--smooth", type=int, default=3)
parser.add_argument("--it", type=int, default=1000)
parser.add_argument("--set-monitor", dest="set_monitor", default=False,
                    action="store_true")
args, _ = parser.parse_known_args()

Dirichlet_boundary = "on_boundary"
qd = args.discretisation == "rt-quad"
gamma = Constant(args.gamma)
zero = Constant((0,0))

# Define base mesh
dp = {"partition": True, "overlap_type": (DistributedMeshOverlapType.VERTEX, 1)}
baseMesh = RectangleMesh(args.N, args.N, 1, args.epsilon, quadrilateral = qd)


# Mesh hierarchy
def before(dm, i):
    for p in range(*dm.getHeightStratum(1)):
        dm.setLabelValue("prolongation", p, i+1)

def after(dm, i):
    for p in range(*dm.getHeightStratum(1)):
        dm.setLabelValue("prolongation", p, i+2)

if args.discretisation == "sv":
    mh = BaryMeshHierarchy(baseMesh, args.nref, callbacks=(before, after),
                           reorder=True, distribution_parameters=dp)
else:
    mh = MeshHierarchy(baseMesh, args.nref, reorder=True, callbacks=(before, after),
                                   distribution_parameters=dp)
mesh = mh[-1]


# Define discretisation
f = zero # RHS

if args.discretisation  == "pkp0":
    V = VectorFunctionSpace(mesh, "CG", args.k)
    u = Function(V)
    v = TestFunction(V)
    F = (
         inner(grad(u), grad(v)) * dx
         + gamma * inner(cell_avg(div(u)), cell_avg(div(v)))*dx
         - inner(f, v) * dx
         )

elif args.discretisation  == "sv":
    V = VectorFunctionSpace(mesh, "CG", args.k)
    u = Function(V)
    v = TestFunction(V)
    F = (
         inner(grad(u), grad(v)) * dx
         + gamma * div(u) * div(v)*dx
         - inner(f, v) * dx
         )

else:
    if args.discretisation == "rt":
        eleu = FiniteElement("RT", mesh.ufl_cell(), args.k)
        V = FunctionSpace(mesh, eleu)
    else:
        eleu = FiniteElement("RTCF", mesh.ufl_cell(), args.k)
        V = FunctionSpace(mesh, eleu)
    u = Function(V)
    v = TestFunction(V)
    sigma = Constant(10) * V.ufl_element().degree()**2
    h = FacetArea(u.ufl_domain())
    n = FacetNormal(u.ufl_domain())
    nu = 1
    def a(u, v):
        return nu * inner(2*sym(grad(u)), grad(v))*dx \
            - nu * inner(avg(2*sym(grad(u))), 2*avg(outer(v, n))) * dS \
            - nu * inner(avg(2*sym(grad(v))), 2*avg(outer(u, n))) * dS \
            + nu * sigma/avg(h) * inner(2*avg(outer(u,n)),2*avg(outer(v,n))) * dS
    F = a(u,v) + gamma * div(u) * div(v) * dx - inner(f, v) * dx

bc = DirichletBC(V, zero, Dirichlet_boundary)

# Define initial conditions
with u.dat.vec_wo as z:
    z.setRandom()
bc.apply(u)

# Solver parameters
multiplicative = args.patch_composition == "multiplicative"
params = solver_parameters.params(args.discretisation, args.relaxation, args.mg, args.N, args.smooth, args.it, multiplicative)
import pprint; pprint.pprint(params)

# Set solver
problem = NonlinearVariationalProblem(F, u, bcs=bc)
solver = NonlinearVariationalSolver(problem, solver_parameters=params)

# Set transfer operators
if args.discretisation == "sv" or args.discretisation == "pkp0":
    print("SchoeberlProlongation set")
    prolongation = SchoeberlProlongation([Constant(1.0) for m in mh], gamma, 2, args.discretisation)
    transfers = {V.ufl_element(): (prolongation.prolong, restrict, inject)}
    transfer_manager = TransferManager(native_transfers = transfers)
    solver.set_transfer_manager(transfer_manager)

if args.set_monitor:
    error = []
    U = FunctionSpace(mesh, "CG", 1)
    pvd = File("output/iterates.pvd")
    erroru = Function(V)
    norm_erroru = Function(U)
    def mymonitor(ksp, it, rnorm):
        error.append(rnorm)
        xbar = solver.snes.getSolution()
        x = ksp.buildSolution()
        with erroru.dat.vec_wo as y:
            xbar.copy(y)
            y.axpy(-1, x)
        norm_erroru.interpolate(dot(erroru,erroru))
        pvd.write(norm_erroru)
    solver.snes.ksp.setMonitor(mymonitor)

solver.solve()

if args.set_monitor:
    from matplotlib import pylab
    pylab.plot(error)
    pylab.yscale('log')
    pylab.grid(True)
    pylab.savefig("output/error.png")
