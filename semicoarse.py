from firedrake import *
import argparse
import solver_parameters

parser = argparse.ArgumentParser(add_help=True)
parser.add_argument("--N", type=int, default=8)
parser.add_argument("--mh", type=str, required=True, choices = ["uniform", "semi"])
parser.add_argument("--solver", type=str, required=True,
    choices = ["jacobi", "line", "lineandstar", "star",
    "mgjacobi", "mggs", "mgline", "mglineandstar"])
parser.add_argument("--epsilon", type=float, default=1)
parser.add_argument("--gamma", type=float, default=1)
parser.add_argument("--nref", type=int, default=1)
parser.add_argument("--smooth", type=int, default=3)
parser.add_argument("--k", type=int, default=1)
args, _ = parser.parse_known_args()

# Construct mesh hierarchy
base1D = IntervalMesh(args.N, 1)
mh1D = MeshHierarchy(base1D, args.nref)
if args.mh == "uniform":
    mh = ExtrudedMeshHierarchy(mh1D, 1, base_layer = args.N, refinement_ratio = 2)
elif args.mh == "semi":
    mh = ExtrudedMeshHierarchy(mh1D, 1, base_layer = int(2**args.nref)*args.N, refinement_ratio = 1)

mesh = mh[-1]

# Define functionspaces
horiz_elt = FiniteElement("CG", interval, args.k)
vert_elt = FiniteElement("CG", interval, args.k)
elt = TensorProductElement(horiz_elt, vert_elt)
V = FunctionSpace(mesh, elt)

u = Function(V)
v = TestFunction(V)

# Define BC
Dirichlet_boundary = "on_boundary"
zero = Constant(0)
bc = DirichletBC(V, zero, Dirichlet_boundary)
f = zero

F = (
     inner(u.dx(0), v.dx(0)) * dx
     + args.epsilon**(-2) * inner(u.dx(1), v.dx(1)) * dx
     - inner(f, v) * dx
     )

# Define initial conditions
with u.dat.vec_wo as z:
    z.setRandom()
bc.apply(u)

# Solver parameters
params = solver_parameters.params(args.solver, args.N, args.smooth)
params["ksp_max_it"] = 1000
params["ksp_gmres_restart"] = 1000
params["ksp_rtol"] = 1.0e-5
params["ksp_monitor"] = None
params["ksp_type"] = "fgmres"
params["patch_pc_patch_partition_of_unity"] = True

import pprint; pprint.pprint(params)

# Set solver
problem = NonlinearVariationalProblem(F, u, bcs=bc)
solver = NonlinearVariationalSolver(problem, solver_parameters=params)

# Set monitor
error = []
pvd = File("output/iterates.pvd")
erroru = Function(V)
def mymonitor(ksp, it, rnorm):
    error.append(rnorm)
    xbar = solver.snes.getSolution()
    x = ksp.buildSolution()
    with erroru.dat.vec_wo as y:
        xbar.copy(y)
        y.axpy(-1, x)
    pvd.write(erroru)
solver.snes.ksp.setMonitor(mymonitor)

# Solve
solver.solve()



# Plot error
from matplotlib import pylab
pylab.plot(error)
pylab.yscale('log')
pylab.grid(True)
pylab.savefig("output/error_" + args.mh + ".png")

def plot_mesh(mesh, name):
    V = FunctionSpace(mesh, "CG", 1)
    u = Function(V)
    File(name + ".pvd").write(u)

for (i,m) in enumerate(mh): plot_mesh(m, "output/m_%d" % i)
