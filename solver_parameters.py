import relaxation

def params(discretisation, relaxation, mg, N, smooth, it, multiplicative):

    # Outer params
    base = {
            "snes_type": "ksponly",
            "snes_monitor": None,
            "ksp_monitor_true_residual" : None,
            "ksp_max_it" : it,
            "ksp_rtol": 1.0e-6,
    }

    # Jacobi
    jacobi = {
            "ksp_type": "richardson",
            "pc_type" : "sor" if multiplicative else "jacobi",
            }

    # Line relaxation
    line = {
            "ksp_type": "fgmres",
            "pc_type" : "python",
            "pc_python_type": "firedrake.PatchPC",
            "patch_pc_patch_save_operators": True,
            "patch_pc_patch_local_type": "additive",
            "patch_pc_patch_symmetrise_sweep": False,
            "patch_pc_patch_construct_type": "python",
            "patch_pc_patch_construct_python_type": "firedrake.PlaneSmoother",
            "patch_pc_patch_construct_ps_sweeps": "0-%d" % (N+1),
            "patch_sub_ksp_type": "preonly",
            "patch_sub_pc_type": "lu",
            }

    # Line and star relaxation
    lineandstar = {
            "ksp_type": "fgmres",
            "pc_type" : "python",
            "pc_python_type": "firedrake.PatchPC",
            "patch_pc_patch_save_operators": True,
            "patch_pc_patch_use_dense_inverse": True,
            "patch_pc_patch_sub_mat_type": "seqdense",
            "patch_pc_patch_local_type": "additive",
            "patch_pc_patch_partition_of_unity": False,
            "patch_pc_patch_symmetrise_sweep": False,
            "patch_pc_patch_construct_type": "python",
            "patch_pc_patch_construct_python_type": "relaxation.LineRelaxationAndStar",
            "patch_pc_patch_construct_ps_sweeps": "0-%d" % (N+1),
            "patch_sub_ksp_type": "preonly",
            "patch_sub_pc_type": "lu",
            }

    # Star relaxation
    star = {
            "ksp_type": "fgmres",
            "pc_type" : "python",
            "pc_python_type": "firedrake.PatchPC",
            "patch_pc_patch_save_operators": True,
            "patch_pc_patch_use_dense_inverse": True,
            "patch_pc_patch_sub_mat_type": "seqdense",
            "patch_pc_patch_local_type": "multiplicative" if multiplicative else "additive",
            "patch_pc_patch_partition_of_unity": False,
            "patch_pc_patch_symmetrise_sweep": multiplicative,
            "patch_pc_patch_precompute_element_tensors": True,
            "patch_sub_ksp_type": "preonly",
            "patch_sub_pc_type": "lu",
            }

    if discretisation == "sv":
            star["patch_pc_patch_construct_type"] = "python"
            star["patch_pc_patch_construct_python_type"] = "relaxation.MacroStar"
            star["patch_pc_patch_construction_MacroStar_sort_order"] = "0+:1-"
    else:
        if multiplicative:
            star["patch_pc_patch_construct_type"] = "python"
            star["patch_pc_patch_construct_python_type"] = "relaxation.Star"
            star["patch_pc_patch_construction_Star_sort_order"] = "0+:1-"

        else:
            star["patch_pc_patch_construct_type"] = "star"
            star["patch_pc_patch_construct_dim"] = 0


    if mg:
        mg_levels = {
            "jacobi": jacobi,
            "star": star,
            "line": line,
            "lineandstar": lineandstar}[relaxation]

        mg_levels = {
            "ksp_max_it": smooth,
            "ksp_monitor": None,
            "ksp_norm_type": "unpreconditioned",
            "ksp_convergence_test": "skip",
            **mg_levels
        }

        params = {
            "ksp_type": "richardson",
            "ksp_richardson_self_scale": False,
            "ksp_norm_type": "unpreconditioned",
            "pc_type": "mg",
            "pc_mg_type": "full",
            "pc_mg_log": None,
            "mg_levels": mg_levels,
            "mg_coarse_pc_type": "python",
            "mg_coarse_pc_python_type": "firedrake.AssembledPC",
            "mg_coarse_assembled": {
                "mat_type": "aij",
                "pc_type": "telescope",
                "pc_telescope_subcomm_type": "contiguous",
                "telescope_pc_type": "lu",
                "telescope_pc_factor_mat_solver_type": "superlu_dist",
            }
        }

    else:
        params = {
            "jacobi": jacobi,
            "line": line,
            "lineandstar": lineandstar,
            "star": star,
        }[relaxation]

    return {**base,**params}
