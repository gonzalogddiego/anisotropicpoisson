from firedrake import *
import argparse
import solver_parameters

parser = argparse.ArgumentParser(add_help=True)
parser.add_argument("--N", type=int, default=8)
parser.add_argument("--epsilon", type=float, default=1)
parser.add_argument("--k", type=int, default=1)
args, _ = parser.parse_known_args()

# Construct mesh hierarchy
base1D = IntervalMesh(args.N, 1)
mesh = ExtrudedMesh(base1D, args.N, args.epsilon)

# Define functionspaces
horiz_elt = FiniteElement("CG", interval, args.k)
vert_elt = FiniteElement("CG", interval, args.k)
elt = TensorProductElement(horiz_elt, vert_elt)
V = FunctionSpace(mesh, elt)
u = Function(V)
v = TestFunction(V)

# Define BC
Dirichlet_boundary = "on_boundary"
zero = Constant(0)
bc = DirichletBC(V, zero, Dirichlet_boundary)
f = zero

F = (
     + inner(grad(u), grad(v)) * dx
     - inner(f, v) * dx
     )

# Define initial conditions
with u.dat.vec_wo as z:
    z.setRandom()
bc.apply(u)

# Solver parameters
params = {
        "snes_type": "ksponly",
        "snes_monitor": None,
        "mat_type" : "aij",
        "ksp_monitor_true_residual" : None,
        "ksp_max_it" : 100,
        "ksp_type": "fgmres",
        "ksp_gmres_restart": 1000,
        "pc_type" : "python",
        "pc_python_type": "firedrake.ASMLinesmoothPC",
        "pc_asm_codims": "0,1",
        "pc_asm_sub_ksp_type": "preonly",
        "pc_asm_sub_pc_type": "lu",
}

import pprint; pprint.pprint(params)

# Set solver
problem = NonlinearVariationalProblem(F, u, bcs=bc)
solver = NonlinearVariationalSolver(problem, solver_parameters=params)

# Solve
solver.solve()
